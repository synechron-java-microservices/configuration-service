# Spring Cloud Config Server - Lab

1. Set up the Spring Boot project with the following configuration from [start.spring.io](https://start.spring.io) portal
   - Spring Version - 2.5.3
   - Java Version - 8
   - Spring Cloud Version - 2020.0.3

2. Add the following dependencies 
```xml
     <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-config-server</artifactId>
        </dependency>
```
3. Create a file called `bootstrap.yaml` under `src/main/resources` directory

4. Add the following code under the `bootstrap.yaml` file 
```yaml
spring:
  application:
    name: configserver

```

5. In the `application.yaml` file add the below configuration

```yml
spring:
  profiles:
    active: git
  cloud:
    config:
      server:
        git:
          uri: https://gitlab.com/synechron-java-microservices/order-service-configuration.git
          repos:
            orderservice:
              uri: https://gitlab.com/synechron-java-microservices/order-service-configuration.git
              clone-on-startup: true

server:
  port: 8888
```


6. Add the `@EnableConfigServer` annotation on the main class

7.  Start the server and verify the configuration for `inventoryservice` and `orderservice` for `default`, `dev` and `qa` profile 
  - URL to verify the `inventoryservice` configuraion - `http://localhost:8888/inventoryservice/dev`
  - URL to verify the `inventoryservice` configuraion - `http://localhost:8888/orderservice/dev` 

8. Add the `encrypt.key` property in `bootstrap.properties` file
